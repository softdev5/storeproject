/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.son.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Product;

/**
 *
 * @author mc-so
 */
public class TestDeleteProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();

        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            stmt.executeUpdate("DELETE from PRODUCT where ID=4");
            conn.commit();

            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT;");

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getInt("price");

                Product pro = new Product(id, name, price);
                System.out.println(pro);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
    }
}
