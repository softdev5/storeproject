/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;

/**
 *
 * @author mc-so
 */
public class DaoProduct implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO PRODUCT (NAME,PRICE) "
                    + "VALUES ('"+object.getName()+"',"+object.getPrice()+");";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()){
                id = result.getInt(1);
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList<Product>();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(id, name, price);
                list.add(pro);
            }
            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        ArrayList<Product> list = new ArrayList<Product>();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT WHERE id="+id);
            if(rs.next()) {
                int pid = rs.getInt("id");
                String name = rs.getString("name");
                double price = rs.getDouble("price");
                Product pro = new Product(pid, name, price);
                return pro;
            }
            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            row = stmt.executeUpdate("DELETE from PRODUCT where ID="+id+"");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE PRODUCT set PRICE ="+object.getPrice()+" where ID="+object.getId()+"");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        DaoProduct dao = new DaoProduct();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1,"Lemon Tea",45.0));
        System.out.println("id = "+id);
        Product last = dao.get(id);
        System.out.println("Last Product = "+last);
        last.setPrice(100);
        int row = dao.update(last);
        Product update = dao.get(id);
        System.out.println("Update Product = "+update);
        dao.delete(id);
        Product del = dao.get(id);
        System.out.println(del);
    }

}

