/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;

import model.User;

/**
 *
 * @author mc-so
 */
public class DaoReceipt implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO RECEIPT (customer_id,user_id,total) "
                    + "VALUES (" + object.getCustomer().getId() + "," + object.getSeller().getId() + "," + object.getTotal() + ");";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail rd : object.getReceiptDetail()) {
                stmt = conn.createStatement();
                String receiptDetail = "INSERT INTO RECEIPT_DETAIL (receipt_id,product_id,price,amount) "
                        + "VALUES (" + object.getId() + "," + rd.getId() + "," + rd.getPrice() + "," + rd.getAmount() + ");";
                stmt.executeUpdate(receiptDetail);
                result = stmt.getGeneratedKeys();
                if (result.next()) {
                    id = result.getInt(1);
                    rd.setId(id);
                }
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList<Receipt>();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Receipt");
            while (rs.next()) {
                int id = rs.getInt("id");
                Date created = rs.getDate("created");
                int cusId = rs.getInt("customer_id");
                int userId = rs.getInt("user_id");
                double total = rs.getDouble("total");
                DaoCustomer cusDao = new DaoCustomer();
                Customer customer = cusDao.get(cusId);
                DaoUser userDao = new DaoUser();
                User seller = userDao.get(userId);
                Receipt pro = new Receipt(id, created, seller, customer);
                pro.setTotal(total);
                list.add(pro);
            }
            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        ArrayList<Receipt> list = new ArrayList<Receipt>();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM RECEIPT WHERE id=" + id);
            int receiptId = 0;
            if (rs.next()) {
                receiptId = rs.getInt("id");
                Date created = rs.getDate("created");
                int cusId = rs.getInt("customer_id");
                int userId = rs.getInt("user_id");
                double total = rs.getDouble("total");
                DaoCustomer cusDao = new DaoCustomer();
                Customer customer = cusDao.get(cusId);
                DaoUser userDao = new DaoUser();
                User seller = userDao.get(userId);
                Receipt receipt = new Receipt(receiptId, created, seller, customer);
                receipt.setTotal(total);

                ResultSet rD = stmt.executeQuery("SELECT * FROM RECEIPT_DETAIL WHERE receipt_id=" + receiptId);
                while (rD.next()) {
                    int rDetailId = rD.getInt("id");
                    int pId = rD.getInt("product_id");
                    double price = rD.getDouble("price");
                    int amount = rD.getInt("amount");
                    DaoProduct proDao = new DaoProduct();
                    Product product = new Product(pId, proDao.get(pId).getName(), proDao.get(pId).getPrice());
                    receipt.addReceiptDetail(product, amount);
                }
                return receipt;
            }
            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            row = stmt.executeUpdate("DELETE from RECEIPT where ID=" + id + "");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Statement stmt = null;
//        Database db = Database.getInstance();
//        conn = db.getConnecttion();
//        int row = 0;
//        try {
//            db.getConnecttion();
//            conn.setAutoCommit(false);
//            stmt = conn.createStatement();
//            row = stmt.executeUpdate("UPDATE RECEIPT set PRICE ="+object.getPrice()+" where ID="+object.getId()+"");
//            conn.commit();
//            stmt.close();
//            conn.close();
//        } catch (SQLException ex) {
//            System.out.println("Unable to open database");
//            System.exit(0);
//        }
//        //System.out.println("Opened database successfully");
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        DaoReceipt dao = new DaoReceipt();
        System.out.println(dao.getAll());

        DaoUser user = new DaoUser();
        DaoCustomer cus = new DaoCustomer();
        DaoProduct product = new DaoProduct();
        User seller = user.get(1);
        Customer customer = cus.get(1);
        Product p1 = product.get(2);
        Product p2 = product.get(4);
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 2);
        receipt.addReceiptDetail(p2, 4);
        System.out.println("p1 =" + p1.getId());
        System.out.println("p1 =" + receipt.getReceiptDetail().get(0).getId());
        System.out.println("Receipt : " + receipt);
        dao.add(receipt);
        System.out.println("Receipt AF: " + receipt);
        System.out.println("All Receipt: " + dao.getAll());
        Receipt rc = dao.get(receipt.getId());
        System.out.println("Receipt : " + rc);
        dao.delete(receipt.getId());

    }

}
