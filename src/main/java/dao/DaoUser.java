/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author mc-so
 */
public class DaoUser implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int id = -1;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO USER (NAME,TEL,PASSWORD) "
                    + "VALUES ('" + object.getName() + "', '" + object.getTel()
                    + "', '" + object.getPassword() + "' );";
            stmt.executeUpdate(sql);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            conn.commit();
            stmt.close();
            conn.close();
            return id;
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        db.close();
        return -1;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList<User> list = new ArrayList<User>();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User user = new User(id, name, tel, password);
                list.add(user);
            }
            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        ArrayList<User> list = new ArrayList<User>();
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER WHERE id=" + id);
            while (rs.next()) {
                int uid = rs.getInt("id");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String password = rs.getString("password");
                User user = new User(uid, name, tel, password);
                return user;
            }
            rs.close();
            stmt.close();
            conn.commit();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            row = stmt.executeUpdate("DELETE from USER where ID=" + id + "");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnecttion();
        int row = 0;
        try {
            db.getConnecttion();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("UPDATE USER set PASSWORD =" 
                    + object.getPassword() + " where ID=" + object.getId() + "");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }

    public static void main(String[] args) {
        DaoUser dao = new DaoUser();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1, "Ten", "087777770", "password"));
        System.out.println("id = " + id);
        User last = dao.get(id);
        System.out.println("Last USER = " + last);
        last.setPassword("0123456");
        int row = dao.update(last);
        User update = dao.get(id);
        System.out.println("Update User = " + update);
        dao.delete(id);
        User del = dao.get(id);
        System.out.println(del);
    }

}
